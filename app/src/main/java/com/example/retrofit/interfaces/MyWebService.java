package com.example.retrofit.interfaces;

import com.example.retrofit.Model.PersonData;
import com.example.retrofit.pojo.ApiResponse;
import com.example.retrofit.pojo.DataBody;
import com.example.retrofit.pojo.Result;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Make_ on 11.01.2017.
 */

public interface MyWebService {
    @GET("/api/?results=15&&inc=name,picture,email,phone,location") // deklarujemy endpoint oraz metodę
    void getData(Callback<DataBody> pResponse);
//    @GET("/wp-content/uploads/2015/08/ArrayWithObjects.txt") // deklarujemy endpoint oraz metodę
//    void getData(Callback<List<PersonData>> pResponse);

//    @POST("/wsexample/") // deklarujemy endpoint, metodę oraz dane do wysłania
//    void postData(@Body DataBody pBody, Callback<DataBody> pResponse);
}
