package com.example.retrofit;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Make_ on 16.01.2017.
 */

public class DetailFragmentLandscape extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_landscape, container, false);
        return view;
    }

    public void setText(String txt) {
        TextView view = (TextView) getView().findViewById(R.id.detailsText);
        view.setText(txt);
        Log.d("why", String.valueOf(txt));
    }

    public void setStreet(String txt) {
        TextView view = (TextView) getView().findViewById(R.id.detailsStreet);
        view.setText(txt);
        Log.d("why", String.valueOf(txt));
    }

    public void setCity(String txt) {
        TextView view = (TextView) getView().findViewById(R.id.detailsCity);
        view.setText(txt);
        Log.d("why", String.valueOf(txt));
    }

    public void setState(String txt) {
        TextView view = (TextView) getView().findViewById(R.id.detailsState);
        view.setText(txt);
        Log.d("why", String.valueOf(txt));
    }

    public void setPostcode(String txt) {
        TextView view = (TextView) getView().findViewById(R.id.detailsPostcode);
        view.setText(txt);
        Log.d("why", String.valueOf(txt));
    }

}
