package com.example.retrofit;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;

/**
 * Created by Make_ on 17.01.2017.
 */

public class DetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // jeżeli użytkownik będzie w orientacji landscape, należy zamknąć aktywność
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            finish();
            return;
        }

        setContentView(R.layout.activity_detail);



        // pobieramy dane wysłane przez aktywność główną
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            // uzyskujemy dane wg. klucza
            String url = extras.getString("msg");
            String street = extras.getString("street");
            String city = extras.getString("city");
            String state = extras.getString("state");
            String postcode = extras.getString("postcode");

            DetailFragmentLandscape detailFragmentLandscape = (DetailFragmentLandscape) getFragmentManager()
                    .findFragmentById(R.id.detailFragment);

            // ustawiamy tekst fragmentu w tej aktywności
            detailFragmentLandscape.setText(url);
            detailFragmentLandscape.setStreet(street);
            detailFragmentLandscape.setCity(city);
            detailFragmentLandscape.setState(state);
            detailFragmentLandscape.setPostcode(postcode);
        }
    }

}
