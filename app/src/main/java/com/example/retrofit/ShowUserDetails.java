package com.example.retrofit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.example.retrofit.Model.PersonData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Make_ on 11.01.2017.
 */

public class ShowUserDetails extends AppCompatActivity implements View.OnClickListener, DetailFragmentPortrait.OverviewFragmentActivityListener {

    //definiowanie widoków
    private TextView textViewPersonName;
    private TextView textViewPersonSurname;
    private TextView textViewPersonEmail;
    private TextView textViewPersonPhone;
    private ImageView imageViewPersonPicture;

    private TextView textViewPersonAge;

    private Button buttonSaveInventory;

    String imageUrl;
    String street;
    String city;
    String state;
    String postcode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_user_details);

        //Inicjalizacja widoków
        textViewPersonName = (TextView) findViewById(R.id.textViewPersonName);
        textViewPersonSurname = (TextView) findViewById(R.id.textViewPersonSurname);
        textViewPersonEmail = (TextView) findViewById(R.id.textViewPersonEmail);
        textViewPersonPhone = (TextView) findViewById(R.id.textViewPersonPhone);
        imageViewPersonPicture = (ImageView) findViewById(R.id.picture);
        //textViewPersonAge = (TextView) findViewById(R.id.textViewPersonAge);

        //Getting intent
        Intent intent = getIntent();

        //Displaying values by fetching from intent
        textViewPersonName.setText(intent.getStringExtra(MainActivity.KEY_PERSON_NAME));
        textViewPersonSurname.setText(intent.getStringExtra(MainActivity.KEY_PERSON_SURNAME));
        textViewPersonEmail.setText(intent.getStringExtra(MainActivity.KEY_PERSON_EMAIL));
        textViewPersonPhone.setText(intent.getStringExtra(MainActivity.KEY_PERSON_PHONE));
        //textViewPersonAge.setText(String.valueOf(intent.getIntExtra(MainActivity.KEY_PERSON_AGE, 0)));
        street = intent.getStringExtra(MainActivity.KEY_PERSON_STREET);
        city = intent.getStringExtra(MainActivity.KEY_PERSON_CITY);
        state = intent.getStringExtra(MainActivity.KEY_PERSON_STATE);
        postcode = intent.getStringExtra(MainActivity.KEY_PERSON_POSTCODE);

        imageUrl = intent.getStringExtra(MainActivity.KEY_PERSON_PICTURE);
        Picasso.with(this).load(imageUrl).into(imageViewPersonPicture);

        //inicjalizowanie przycisku dodajacego do bd
        buttonSaveInventory = (Button) findViewById(R.id.buttonSaveInventory);

        //dodawanie listenera do przycisku
        buttonSaveInventory.setOnClickListener(this);

    }

    private List<PersonData> getAll() {
        //Getting all items stored in Inventory table
        return new Select()
                .from(PersonData.class)
                .orderBy("Name ASC")
                .execute();
    }


    private void saveInventory() {
        //Getting name from editText
        String name = textViewPersonName.getText().toString();

        PersonData person3 = new Select("p.Id")
                .from(PersonData.class).as("p")
                .where("p.name = ?", name)
                .executeSingle();

        //Checking if name is blank
        if (person3 != null) {
            Toast.makeText(this, "Osoba już jest na liście kandydatów", Toast.LENGTH_LONG).show();
            return;
        }

        //If name is not blank creating a new Inventory object
        PersonData personData = new PersonData();
        //Adding the given name to inventory name
        personData.setName(name);
        //Saving name to sqlite database
        personData.save();
        personData.getId();
        //inventoryItems.add(name);

        Toast.makeText(this, "Dodano poprawnie", Toast.LENGTH_LONG).show();

    }


    @Override
    public void onClick(View v) {
        saveInventory();
    }

    // ta metoda pochodzi z OverviewFragmentActivityListener
    @Override
    public void onItemSelected(String msg) {
        DetailFragmentLandscape fragment = (DetailFragmentLandscape) getFragmentManager()
                .findFragmentById(R.id.detailFragment);

        // sprawdzamy czy fragment istnieje w tej aktywności
        // znaczy, że jesteśmy w trybie landscape
        if (fragment != null && fragment.isInLayout()) {
            // ustawiamy teskt we fragmencie
            fragment.setText(msg);
            fragment.setStreet(street);
            fragment.setCity(city);
            fragment.setState(state);
            fragment.setPostcode(postcode);
        } else {
            // w trybie portrait wywołujemy drugą aktywność
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            intent.putExtra("msg", msg);
            intent.putExtra("street", street);
            intent.putExtra("city", city);
            intent.putExtra("state", state);
            intent.putExtra("postcode", postcode);
            startActivity(intent);
        }
    }
}
