package com.example.retrofit.helpers;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.retrofit.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Make_ on 17.01.2017.
 */

public class CustomList extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] web;
    private final String[] imageId;
    public CustomList(Activity context, String[] web, String[] imageId) {
        super(context, R.layout.simple_list, web);
        this.context = context;
        this.web = web;
        this.imageId = imageId;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.simple_list, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);
        txtTitle.setText(web[position]);

        //imageView.setImageResource(imageId[position]);

        Picasso.with(context)
                .load(imageId[position])
                .into(imageView);
        return rowView;
    }
}
