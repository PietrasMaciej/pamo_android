package com.example.retrofit.helpers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Make_ on 17.01.2017.
 */

public class StingListDeserializer implements JsonDeserializer<StringList> {

    @Override
    public StringList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext
            context) throws JsonParseException {
        StringList value = new StringList();
        if (json.isJsonArray()) {
            for (JsonElement element : json.getAsJsonArray()) {
                value.add(element.getAsString());
            }
        } else {
            value.add(json.getAsString());
        }
        return value;
    }

}
