package com.example.retrofit.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Make_ on 12.01.2017.
 */

public class ApiResponse {
    private List<DataBody> objects = new ArrayList<DataBody>();

    public List<DataBody> getObjects() {
        return objects;
    }
}
