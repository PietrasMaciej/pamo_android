package com.example.retrofit.DB;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

/**
 * Created by Make_ on 12.01.2017.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //Inicializowanie Active Android
        ActiveAndroid.initialize(this);
    }

}
