package com.example.retrofit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.example.retrofit.Model.PersonData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Make_ on 18.01.2017.
 */

public class DatabaseList extends AppCompatActivity {

    //Tworzenie widoków dla bd
    //private EditText editTextInventoryName;
    private Button buttonSaveInventory;
    private ListView listViewInventories;

    //Array list do przechowywania wszystkich rekordow bd
    private ArrayList<String> inventoryItems;

    //Adapter do listview db
    private ArrayAdapter inventoryItemsAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.database_persons);


        //Getting intent
        Intent intent = getIntent();

//        //Displaying values by fetching from intent
//        textViewPersonName.setText(intent.getStringExtra(MainActivity.KEY_PERSON_NAME));
//        textViewPersonSurname.setText(intent.getStringExtra(MainActivity.KEY_PERSON_SURNAME));
//        textViewPersonEmail.setText(intent.getStringExtra(MainActivity.KEY_PERSON_EMAIL));
//        textViewPersonPhone.setText(intent.getStringExtra(MainActivity.KEY_PERSON_PHONE));
//        //textViewPersonAge.setText(String.valueOf(intent.getIntExtra(MainActivity.KEY_PERSON_AGE, 0)));

        //inicjalizowanie widokow bd
        //editTextInventoryName = (TextView) findViewById(R.id.editTextInventoryName);
        buttonSaveInventory = (Button) findViewById(R.id.buttonSaveInventory);
        listViewInventories = (ListView) findViewById(R.id.listViewInventories);

        //inicjalizowanie arraylist
        inventoryItems = new ArrayList<>();

        //wywolywanie metody do wyswietlenia listy rekordow
        showInventoryList();
        deletePerson();

    }

    private List<PersonData> getAll() {
        //Getting all items stored in Inventory table
        return new Select()
                .from(PersonData.class)
                .orderBy("Name ASC")
                .execute();
    }

    private void showInventoryList() {
        //Creating a list and getting all inventories from the method
        List<PersonData> inventories = getAll();

        //Adding all the items of the inventories to arraylist
        for (int i = 0; i < inventories.size(); i++) {
            PersonData inventory = inventories.get(i);
            inventoryItems.add(inventory.getName());
        }

        //Creating our adapter
        inventoryItemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, inventoryItems);

        //Adding adapter to listview
        listViewInventories.setAdapter(inventoryItemsAdapter);

    }

    private void updateInventoryList() {
        inventoryItemsAdapter.notifyDataSetChanged();
    }

    private void deletePerson() {
        listViewInventories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //TextView textv=(TextView)findViewById(R.id.simpleList);
                //String item = ((TextView)view).getText().toString();
                //int indexToRemove = inventoryItems.indexOf(item);
                Object person = adapterView.getAdapter().getItem(i);
                //long indexOfPerson = adapterView.getAdapter().getItemId(i);
                int indexToRemove = inventoryItems.indexOf(person);

                PersonData person2 = new Select("p.Id")
                        .from(PersonData.class).as("p")
                        .where("p.name = ?", person)
                        .executeSingle();

                PersonData personData = PersonData.load(PersonData.class, person2.getId());
                personData.delete();

//                Toast.makeText(getApplicationContext(),"You Clicked "+person2,Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(),"Usunięto kandydata",Toast.LENGTH_SHORT).show();
                inventoryItems.remove(indexToRemove);
                inventoryItemsAdapter.notifyDataSetChanged();
            }
        });
    }


}
