package com.example.retrofit;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.retrofit.Model.PersonData;
import com.example.retrofit.helpers.CustomList;
import com.example.retrofit.interfaces.MyWebService;
import com.example.retrofit.pojo.ApiResponse;
import com.example.retrofit.pojo.DataBody;
import com.example.retrofit.pojo.Name;
import com.example.retrofit.pojo.Picture;
import com.example.retrofit.pojo.Result;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.AndroidLog;
import retrofit.client.Response;
import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;

public class MainActivity extends AppCompatActivity implements ListView.OnItemClickListener {


    //Root URL z naszego web servicu
    public static final String ROOT_URL = "https://randomuser.me/";
    //public static final String ROOT_URL = "http://www.instinctcoder.com/";

    //Stringi to powiązywania intencji, które będą wykorzystywane to wysyłania danych do drugiej aktywności
    public static final String KEY_PERSON_NAME = "key_person_name";
    public static final String KEY_PERSON_SURNAME = "key_person_surname";
    public static final String KEY_PERSON_EMAIL = "key_person_email";
    public static final String KEY_PERSON_PHONE = "key_person_phone";
    public static final String KEY_PERSON_PICTURE = "key_user_picture";
    //public static final String KEY_PERSON_AGE = "key_user_age";
    public static final String KEY_PERSON_STREET = "key_user_street";
    public static final String KEY_PERSON_CITY = "key_user_city";
    public static final String KEY_PERSON_STATE = "key_user_state";
    public static final String KEY_PERSON_POSTCODE = "key_user_postcode";

    //TextView gender;

    //Deklarowanie ImageView
    private ImageView imageView;

    //List view do pokazywania danych
    private ListView listView;

    //Lista typu Result, która przechowuje pobrane dane zgodnie z modelem
    private List<Result> users;
    private Result user;

    // tag, który jest wykorzystany do logowania
    private static final String CLASS_TAG = "Klasa MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //gender = (TextView) findViewById(R.id.gender);

        //Inicjalizowanie ImageView
        imageView = (ImageView) findViewById(R.id.imageView);

        //Inicjalizowanie listview
        listView = (ListView) findViewById(R.id.listViewUsers);

        //Wywoływanie metody, która pobierze dane
        getUsers();

        //Setting onItemClickListener to listview
        listView.setOnItemClickListener(this);

        // ustawiamy przycisk pobierający dane metodą GET
        findViewById(R.id.button_get).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getUsers();

                } catch (Exception e) {
                    Log.d(CLASS_TAG, e.toString());
                }
            }
        });
    }

    private void getUsers(){

        //Podczas gdy aplikacja pobiera dane, wyswietlany jest progress dialog
        final ProgressDialog loading = ProgressDialog.show(this,"Pobieranie danych","Proszę czekać...",false,false);

        //Tworzenie restowego adaptera
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL).setLog(new AndroidLog("uczem siem"))
                .setEndpoint(ROOT_URL)
                .build();

        //Tworzenie obiektu naszego interfejsu api
        MyWebService api = adapter.create(MyWebService.class);

        api.getData(new Callback<DataBody>() {
            @Override
            public void success(DataBody list, Response response) {

                //Odwoływanie ładującego się progressbara
                loading.dismiss();

                //Przechowywanie danych w naszej liście
                users = list.getResults();
                for(int i=0; i<users.size(); i++){
                    Name name = users.get(i).getName();
                    Log.d(CLASS_TAG, "imie: " + name.getFirst());
                }
                //Log.d(CLASS_TAG, String.valueOf(users.get(0).getPicture()));

                //Metoda wywoływana w celu wyświetlenia listy
                showList();
                //Toast.makeText(getApplicationContext(), response.getBody().toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(CLASS_TAG, error.getLocalizedMessage());
            }
        });
    }

    //Metoda wyświetlająca listę
    private void showList(){
        //String array to store all the book names
        //String[] items = new String[users.size()];
        String[] items = new String[users.size()];
        String[] pictures = new String[users.size()];

        //Iterowanie po całej liście w celu pozyskania wszystkich imion itd.
        for(int i=0; i<users.size(); i++){
            //Przechowywanie imion w tablicy typu string
            //items[i] = users.get(i).getName();
            Name name = users.get(i).getName();
            Picture picture = users.get(i).getPicture();
            items[i] = name.getFirst();
            pictures[i] = picture.getLarge();
            //Log.d(CLASS_TAG, "showList: " + users.get(i));
        }

        //ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.simple_list,items);
        //Tworzenie customlist adaptera dla list view
        CustomList adapter = new CustomList(MainActivity.this, items, pictures);

        //Ustawianie adaptera na listview
        listView.setAdapter(adapter);
    }

    //Ta metoda zostanie wywołana po naciśnieciu elementu na liście
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Tworzenie intencji
        Intent intent = new Intent(this, ShowUserDetails.class);

        //Pozyskiwanie żądanego obiektu z listy
        Result result = users.get(position);

        //Dodawanie odpowiednich atrybutów osoby do intencji
        intent.putExtra(KEY_PERSON_NAME,result.getName().getFirst());
        intent.putExtra(KEY_PERSON_SURNAME,result.getName().getLast());
        intent.putExtra(KEY_PERSON_EMAIL,result.getEmail());
        intent.putExtra(KEY_PERSON_PHONE,result.getPhone());
        intent.putExtra(KEY_PERSON_PICTURE,result.getPicture().getLarge());
        intent.putExtra(KEY_PERSON_STREET,result.getLocation().getStreet());
        intent.putExtra(KEY_PERSON_CITY,result.getLocation().getCity());
        intent.putExtra(KEY_PERSON_STATE,result.getLocation().getState());
        intent.putExtra(KEY_PERSON_POSTCODE,result.getLocation().getPostcode());
        //intent.putExtra(KEY_PERSON_AGE,result.getAge());

        //Uruchamianie innej aktywności w celu pokazania szczegółowych danych o osobie
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.database) {
            Intent intent = new Intent(MainActivity.this, DatabaseList.class);
            startActivity(intent);
        }
        return false;
    }

    static class DynamicJsonConverter implements Converter {

        @Override public Object fromBody(TypedInput typedInput, Type type) throws ConversionException {
            try {
                InputStream in = typedInput.in(); // convert the typedInput to String
                String string = fromStream(in);
                in.close(); // we are responsible to close the InputStream after use

                if (String.class.equals(type)) {
                    return string;
                } else {
                    return new Gson().fromJson(string, type); // convert to the supplied type, typically Object, JsonObject or Map<String, Object>
                }
            } catch (Exception e) { // a lot may happen here, whatever happens
                throw new ConversionException(e); // wrap it into ConversionException so retrofit can process it
            }
        }

        @Override public TypedOutput toBody(Object object) { // not required
            return null;
        }

        private static String fromStream(InputStream in) throws IOException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append("\r\n");
            }
            return out.toString();
        }
    }
}
