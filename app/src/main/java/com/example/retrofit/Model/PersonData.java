package com.example.retrofit.Model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Make_ on 12.01.2017.
 */
@Table(name = "PersonData")
public class PersonData extends Model {
    @Column(name = "name")
    @SerializedName("name")
    @Expose
    private String name;
    //@Column(name = "age")
    @SerializedName("age")
    @Expose
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
